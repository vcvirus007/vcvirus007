module.exports = function(grunt) {
    /* 
        npm install grunt,
        Copy gruntfile.js and package.json file root folder change src
        npm install grunt-contrib-uglify,grunt-contrib-cssmin,grunt-contrib-htmlmin or
        npm install
        grunt --force
    */
    var mini = 'Production';
    grunt.initConfig({
        uglify: {
            my_target: {
                files: grunt.file.expandMapping(['js/**/*.js','assets/**/*.js'],
                    mini + '/', {
                        // rename: function(destBase, destPath) {
                        //     return destBase + destPath.replace('.js', '.js');
                        // }
                    })
            }
        },
        cssmin: {
            my_target: {
                files: [{
                    'src': ['assets/**/*.css'],
                    'dest': mini + '/',
                    'expand': true
                        // 'ext': '.css'
                }]
            }
        },
        htmlmin: { // Task
            dist: { // Target
                options: { // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: grunt.file.expandMapping(['*.html', 'partials/**/*.html'],
                    mini + '/', {
                        // rename: function(destBase, destPath) {
                        //     return destBase + destPath.replace('.html', '.html');
                        // }
                    })
            }
        },
        imagemin: { // Task 
            dynamic: { // Another target 
                options: { // Target options 
                    expand: true, // Enable dynamic expansion 
                    progressive: true
                },
                files: grunt.file.expandMapping(['images/**/*.{png,jpg,gif}'],
                    mini + '/', {
                        // rename: function(destBase, destPath) {
                        //     return destBase + destPath.replace('.html', '.html');
                        // }
                    })
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('default', [
        'uglify', 'cssmin', 'htmlmin', 'imagemin'
    ]);
};
