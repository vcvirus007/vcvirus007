angular.module('ETCS_APP').config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', 'JS_REQUIRES',
    function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, JS_REQUIRES) {
        $urlRouterProvider.otherwise('/app/home');
        $stateProvider

        // HOME STATES AND NESTED VIEWS 
            .state('app', {
                url: '/app',
                templateUrl: 'partials/app.html',
                resolve: {
                    b: ['apiService', function(apiService) {
                        return apiService.loadServices();
                    }],
                    a: ['apiService', 'b', function(apiService, b) {
                        return apiService.loadLatestNews();
                    }]
                },
                abstract: true
            })
            .state('app.home', {
                url: '/home',
                templateUrl: 'partials/home.html'
            })
            .state('app.services', {
                url: '/services',
                templateUrl: 'partials/services.html',
                resolve: {
                    a: [function() {
                        return angular.element(document.body).injector().invoke(loadSequence('ServicesController')['deps'])
                    }]
                }
            })
            .state('app.singleservice', {
                url: '/singleservice/:id',
                templateUrl: 'partials/single-service.html',
                resolve: {
                    a: [function() {
                        return angular.element(document.body).injector().invoke(loadSequence('SingleServiceController')['deps'])
                    }]
                }
            })
            // ABOUT PAGE AND MULTIPLE NAMED VIEWS 
            .state('app.about', {
                url: '/about',
                templateUrl: 'partials/about.html'
            }).state('app.contact', {
                url: '/contact',
                templateUrl: 'partials/contact.html'
            });

        function loadSequence() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q',
                    function($ocLL, $q) {
                        var promise = $q.when(1);
                        for (var i = 0, len = _args.length; i < len; i++) {
                            promise = promiseThen(_args[i]);
                        }
                        return promise;

                        function promiseThen(_arg) {
                            if (typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function() {
                                    var nowLoad = requiredData(_arg);
                                    if (!nowLoad)
                                        return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    return $ocLL.load(nowLoad);
                                });
                        }

                        function requiredData(name) {
                            if (JS_REQUIRES.modules)
                                for (var m in JS_REQUIRES.modules)
                                    if (JS_REQUIRES.modules[m].name && JS_REQUIRES.modules[m].name === name)
                                        return JS_REQUIRES.modules[m];
                            return JS_REQUIRES.scripts && JS_REQUIRES.scripts[name];
                        }
                    }
                ]
            };
        }
    }
]);
