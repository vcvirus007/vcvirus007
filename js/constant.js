'use strict';

/**
 * Config constant
 */

angular.module('ETCS_APP').constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Controllers
        'ServicesController': 'js/controllers/ServicesController.js',
        'SingleServiceController': 'js/controllers/SingleServiceController.js'
    },
    //*** angularJS Modules
    modules: []
});
