angular.module('ETCS_APP').factory('apiService', ['$q', '$timeout', 'dataService', '$http',
    function($q, $timeout, dataService, $http) {
        console.log('DataService');
        return {
            loadServices: function() {
                var services = $q.defer();
                $http.get("js/controllers/SingleService.json").then(function(response) {
                    dataService.services = response.data;
                    services.resolve(response);
                }, function(error) {
                    services.reject(error);
                })
                return services.promise;
            },
            loadLatestNews: function() {
                var services = $q.defer();
                $http.get("js/controllers/LatestNews.json").then(function(response) {
                    dataService.latestNews = response.data;
                    services.resolve(response);
                }, function(error) {
                    services.reject(error);
                })
                return services.promise;
            }
        }
    }
]);
