(function() {

    singleServiceController.$inject = ["$state", "$scope", "$http", "dataService"];

    function singleServiceController($state, $scope, $http, dataService) {
        $scope.name = $state.params.id;
        $scope.relatedServ = [];
        var data = dataService.services;
        for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < data[i].sub_serv.length; j++) {
                if ($scope.name == data[i].sub_serv[j].name) {
                    $scope.response = data[i].sub_serv[j];
                    $scope.relatedServ = data[i].sub_serv;
                    break;
                }
            }
        }
    }
    angular.module('ETCS_APP').controller('SingleServiceController', singleServiceController);
})()
