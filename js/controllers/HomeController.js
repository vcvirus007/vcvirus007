(function() {

    homeController.$inject = ["$state", "$scope", "$http", "dataService"];

    function homeController($state, $scope, $http, dataService) {
        $scope.$on('$stateChangeSuccess', serviceLim);
        $scope.services = dataService.services;
        $scope.latestNews = dataService.latestNews;
        $scope.subServ = [];
        for (var i = 0; i < $scope.services.length; i++) {
            Array.prototype.push.apply($scope.subServ, $scope.services[i].sub_serv);
        }

        function serviceLim() {
            console.log($state.current);
            if ($state.current.name == "app.home") {
                $scope.serviceLen = 6;
            } else if ($state.current.name == "app.services") {
                $scope.serviceLen = $scope.subServ.length;
            }
        }
    }
    angular.module('ETCS_APP').controller('HomeController', homeController);
})()
