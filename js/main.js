angular.module('ETCS_APP', ['ui.router', 'oc.lazyLoad']).run(['$rootScope',
    function($rootScope) {
        $rootScope.$on("$stateChangeSuccess", function(event, next, current) {
            $("html, body").animate({
                scrollTop: 0
            }, 800);
        });
    }
]);
